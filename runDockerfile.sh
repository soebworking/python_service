#!/bin/bash

#Enable debug mode in shell script
#set -x

scriptUsage(){

  echo ""
  echo ""
  echo ""
  echo "runDockerfile.sh [option] dev | stg | prod"
  echo "e.g., runDockerfile.sh dev"
  echo ""
  echo ""
  echo ""
  exit # stop script execution
}



if [ -z "$1" ]
  then
    echo "No argument supplied"
    scriptUsage
fi



##
# to have environments [dev|stg|prod] specific .env file
# for demo purpose, creating different sqlite db for ech environments
# dev =>  SQLALCHEMY_DATABASE_URL=sqlite:///./sql_app_dev.db
# stg =>  SQLALCHEMY_DATABASE_URL=sqlite:///./sql_app_stg.db
# prod => SQLALCHEMY_DATABASE_URL=sqlite:///./sql_app_prod.db
#
#
# NOTE: REAL WORLD APPLICATION, WE CAN INSTALL ANY OF THE DATABASE (LIKE, postgresql, MANGODB, ORACLE)
# IN THE LINUX IMAGE / ATTACH TO CONTAINER AS [Container networking] AND APPLICATION CAN CONNECT WITH DATABASE
# OR CLOUD DATABASE
##

if [ "$1" = "dev" ]; then
  echo "SQLALCHEMY_DATABASE_URL=sqlite:///./sql_app_dev.db" > .env
fi

if [ "$1" = "stg" ]; then
  echo "SQLALCHEMY_DATABASE_URL=sqlite:///./sql_app_stg.db" > .env
fi

if [ "$1" = "prod" ]; then
  echo "SQLALCHEMY_DATABASE_URL=sqlite:///./sql_app_prod.db" > .env
fi

docKerImageName="python_service_image-$1"
dockerImageVersionTag="v1"
dockerContainerName="python_service_container"


chkDockerImg=$( docker images -q "${docKerImageName}" ) # Get docker image id e.g., IMAGE ID = f287527f2eef
echo "Docker IMAGE ID =  ${chkDockerImg}"


if [[ -n "${chkDockerImg}" ]]; then
echo "image exists. deleting exising image, ${docKerImageName} with IMAGE ID = ${chkDockerImg}"
# IF image already exist then delete, as we're rebuilding the same version of the image
#stop docker container
docker stop ${dockerContainerName}
docker rmi -f ${chkDockerImg}
else
echo "No docker image exists with name: ${docKerImageName}"
echo ""
echo "Creates new docker image with name: ${docKerImageName}"
echo ""
fi


#Image – A Docker Image is a file that is essentially a snapshot of a container. You can create a container by running a Docker Image
dockerCommand="docker build -t ${docKerImageName}:${dockerImageVersionTag} --build-arg buildDate=$(date +'%Y-%m-%d.%H.%m.%S') --target $1 ."
echo ""
echo ""
echo "about to start execution of the command::   ${dockerCommand}"
echo ""
echo ""

#Execute docker command
$(eval "$dockerCommand")



# Once docker image is ready, we can test in detach mode [Run container in background]
# -p <container port>:<host port>
#We are exposing the port on the nginx server which is port 80 to the port 80 on the Docker Host.
#[default] file has port: 80
#docker run -d -p 80:80 ${docKerImageName}:${dockerImageVersionTag}

#create docker container
#Container – These are what docker is built on. They encapsulate an application and all of its libraries and dependencies, so it can be run anywhere Docker is installed.
# There are other containerization platforms, like Kubernetes and AWS Elastic Container Service (ECS).

echo "built docker images and proceeding to delete existing container"
echo "Deleted the existing docker container, If exists!"
docker container rm -f ${dockerContainerName} #in case container does not exist script execution won't stop.
echo ""

echo "Creating new container with name: ${dockerContainerName}"
docker create --name ${dockerContainerName} -p 80:80 ${docKerImageName}:${dockerImageVersionTag}

#run docker container
docker start ${dockerContainerName}
echo ""
echo "Docker container ${dockerContainerName} started, please check http://localhost/"
echo ""
echo "http://localhost/docs"
echo ""

# delete generated .env file
rm -v ./.env

#$? check value whether 0 or not, 0 means last command successfully executed
echo "status #: $?"