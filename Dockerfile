
############################################################################################################
#
#                   DEVELOPMENT ENVIRONMENT :: dev
#
#
############################################################################################################

# using ubuntu
FROM ubuntu:20.04 AS dev

# avoid stuck build due to user prompt, expect input like set geo graphy location, etc.. during installation
ARG DEBIAN_FRONTEND=noninteractive

#Define a variable that users can pass at build-time to the builder with the docker build.
#The second line, starting with ARG, defines an argument that is expected during the build time.
ARG buildDate

#Add metadata to an image. A LABEL is a key-value pair.
LABEL buildDate=$buildDate
LABEL developer="Mohamedsoeb_Ahmednashirwala"
LABEL maintainer="Docker Maintainers soeb.working@gmail.com"


#custom python installation, specific to version 3.9
RUN apt-get update && apt-get install --no-install-recommends -y python3.9 python3.9-dev python3.9-venv python3-pip python3-wheel build-essential && \
	apt-get clean && rm -rf /var/lib/apt/lists/*

# create and activate virtual environment
# using final folder name to avoid path issues with packages
RUN python3.9 -m venv /home/user/venv
ENV PATH="/home/user/venv/bin:$PATH"

#Set the current working directory to /src.
#This is where we'll put the requirements.txt file and the app directory.
WORKDIR /src

#Copy the file with the requirements to the /src directory.
#Copy only the file with the requirements first, not the rest of the code.
COPY ./requirements.txt /src/requirements.txt

#Install the package dependencies in the requirements file.
#The --no-cache-dir option tells pip to not save the downloaded packages locally, as that is only if pip was going to be run again to install the same packages, but that's not the case when working with containers.
#The --upgrade option tells pip to upgrade the packages if they are already installed.
#Using the cache in this step will save you a lot of time when building the image again and again during development, instead of downloading and installing all the dependencies every time.
RUN pip3 install --no-cache-dir --upgrade -r requirements.txt

#Copy the ./ directory inside the /src directory.
#As this has all the code which is what changes most frequently the Docker cache won't be used for this or any following steps easily.
#So, it's important to put this near the end of the Dockerfile, to optimize the container image build times.
COPY . /src

#run the application using uvicorn command
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]



